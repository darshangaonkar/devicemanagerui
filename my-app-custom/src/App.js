import React, { Component } from 'react';
import {
  Route,
  BrowserRouter,
  Switch
} from "react-router-dom";
import Header from './components/Header';
import Sidebar from './components/SideBar';
import DashBoard from './components/DashBoard';
import PurchaseForm from './components/PurchaseForm';
import DeviceList from './components/DeviceList';



class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Header/>
          <Sidebar/>
          <Switch>
          <Route exact path="/purchaseForm" component={PurchaseForm} />
          <Route path="/getAllDeviceList" component={DeviceList} />  
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
