import React, { Component } from 'react';
import { Link } from "react-router-dom";
import '../css/Header.css';

class Header extends Component {
    render() {
        return (
            <div className="header">
                <div>
                    <Link to="/" className="logo">DM</Link>
                </div>
                <div className="header-right">
                    <a href="#home">Home</a>
                    <a href="#contact">Contact</a>
                    <a href="#about">About</a>
                </div>
            </div>
        );
    }
}

export default Header;
