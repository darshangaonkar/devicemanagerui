import React, { Component } from 'react';
import { Table, Button } from 'react-bootstrap';

import "../css/styles.css";
import '../css/DeviceList.css';


class DeviceList extends Component {
    constructor(props) {
        console.log("Props - ", props);
        super(props);
        this.state = {
            modalVisible: false
        };
        this.openModal = this.openModal.bind(this);
    }

    openModal() {
        console.log("Open modal called ", this.state.modalVisible);
        const modalVisible = !this.state.modalVisible;
        this.setState({
            modalVisible
        });
    }

    render() {

        let styles = this.state.modalVisible
            ? { display: "block" }
            : { display: "none" };
        return (

            <div className="w3-container w3-white">

                <Table striped bordered hover className="deviceListplacing">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Device Id</th>
                            <th>Device Name</th>
                            <th>Device Cost</th>
                            <th>Sales</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>SA1</td>
                            <td>IC841</td>
                            <td>400</td>
                            <td> <Button variant="primary" type="submit" onClick={this.openModal}>Sale
                             </Button></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SB2</td>
                            <td>7svRTE</td>
                            <td>800</td>
                            <td> <Button variant="primary" type="submit" onClick={this.openModal}>Sale
                             </Button></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>SB5</td>
                            <td>Amp901IC</td>
                            <td>1000</td>
                            <td> <Button variant="primary" type="submit" onClick={this.openModal}>Sale
                             </Button></td>
                        </tr>
                    </tbody>


                </Table>


                <div
                    id="myModal"
                    className="modal fade in"
                    role="dialog"
                    style={styles}
                >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button
                                    type="button"
                                    onClick={this.openModal}
                                    className="close"
                                >
                                    &times;
                </button>
                                <h4 className="modal-title">Sales Information</h4>
                            </div>
                            <div className="modal-body">
                                <p>Sale process completed</p>
                            </div>
                            <div className="modal-footer">
                                <button
                                    onClick={this.openModal}
                                    type="button"
                                    className="btn btn-default"
                                >
                                    Close
                </button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>



        );
    }
}

export default DeviceList;
