import React, { Component } from 'react';
import { Link } from "react-router-dom";
import '../css/SideBar.css';

class Sidebar extends Component {
    render() {
        return (
            <div className="w3-sidebar w3-container w3-teal w3-bar-block sideheader">
                <h3 className="w3-bar-item">Buyer</h3>
                <Link to="/purchaseForm" className="w3-bar-item w3-button ">Purchase Form</Link>
                <Link to="/getAllDeviceList" className="w3-bar-item w3-button">Other Forms</Link>
            </div>
        );
    }
}

export default Sidebar;



