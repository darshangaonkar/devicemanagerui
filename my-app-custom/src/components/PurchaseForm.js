import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import '../css/PurchaseForm.css';
import axios from 'axios';

class PurchaseForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            deviceName: '',
            deviceCost: '',
        }

    }

    handleDeviceFormClick = e => {
        e.preventDefault();
        console.log(this.state)
        axios.post('http://localhost:8080/buy/addboughtDevice', this.state)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })

    }

    changehandler = e => {
        this.setState({ [e.target.name]: e.target.value })
    }


    render() {

        const { deviceName, deviceCost } = this.state;
        return (

            <div className="w3-container w3-white">
                <Form className="placing" onSubmit={this.handleDeviceFormClick}>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Device Name</Form.Label>
                        <Form.Control type="text"  placeholder="Aend%6IC"
                        name = "deviceName"
                            value={deviceName}
                            onChange={this.changehandler} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Device cost</Form.Label>
                        <Form.Control type="text" placeholder="400"
                        name = "deviceCost"
                            value={deviceCost}
                            onChange={this.changehandler} />
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit
                             </Button>
                </Form>

            </div>


        );
    }
}

export default PurchaseForm;
